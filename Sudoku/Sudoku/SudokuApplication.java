package Sudoku;

public class SudokuApplication {
    public static void main(String[] args) {
        SudokuSolver solver = new SudokuSolver();
        new SudokuGUI(solver);

        // fixa med GitLab

        /*
        Skriv testfall med JUnit för metoderna i din sudokuklass. Bland testme-
        toderna ska ingå lösning av ett tomt sudoku, lösning av sudokut i figur 1 samt försök att
        lösa lämpliga fall av olösliga sudokun. Alla publika metoder ska ha anropats åtminstone
        en gång i testklassen.
        Interfacet SudokuSolver ska användas som typ för sudokulösaren i testklassen. Det är bara
        när sudokulösarobjektet skapas som du ska använda ditt eget klassnamn.
         */

        // kolla checklista och få allt att stämma där

        // Prova gärna att skapa javadoc-filer och kontrollera att dokumentationen ser bra ut.

        // läs instruktionsdokument för redovisning av labb

        // läs igenom instruktionerna en sista gång
    }
}
